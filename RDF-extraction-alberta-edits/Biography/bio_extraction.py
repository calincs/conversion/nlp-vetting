#!/usr/bin/python3
import re
from bs4 import BeautifulSoup
import rdflib

from biography import Biography
from log import *
from Utils.utilities import *
from Utils import utilities
import culturalForm as cf
import education
import location
import other_contexts
import occupation

# import personname
import lifeInfo
import birthDeath

import csv
import itertools

"""
This is a possible temporary main script that creates the biography related triples
TODO:
add documentation
implement personname
"""

logger = utilities.config_logger("biography")


def hasNumbers(inputString):
    return any(char.isdigit() for char in inputString)


def reorder_name(name):
    split = name.split(",")
    reorder = ""
    if ",," in name:
        last = name.split(",,")[-1]
        start = name.split(",,")[0].split(",")
        if not start:
            if last:
                return last[0]
            else:
                return name
        elif len(start) == 1:
            reorder = start[0]
        else:
            reorder = start[1].strip() + " " + start[0].strip()
        if not hasNumbers(last):
            reorder = last.strip() + " " + start[1].strip()
    else:
        if not split:
            return name
        elif len(split) == 1:
            return split[0]
        else:
            reorder = split[1].strip() + " " + split[0].strip()
    return reorder.lstrip(",").strip()


def tab_join(a, b, c):
    if a and b and c:
        return "[" + a + "]" + " " + b + " " + "[" + c + "]" + "\n"
    else:
        return ""

def clean_name(standard_name):
    standard_name = re.sub(":+ +[bB]iography", "", standard_name)
    standard_name = re.sub("\:", " ", standard_name)
    standard_name = re.sub("[0-9]{0,4} *- *[0-9]{0,4}", " ", standard_name)
    standard_name = re.sub("[db]\. *[0-9]{1,4}", " ", standard_name)
    standard_name = re.sub("[0-9]{4}", " ", standard_name)
    standard_name = re.sub(" +", " ", standard_name)
    return standard_name.strip().strip(",")

def swap_name(standard_name):
    standard_name_list = standard_name.split(",")
    if len(standard_name_list) > 1:
        standard_name = " ".join(standard_name_list[1:]) + " " + standard_name_list[0]
    return clean_name(standard_name)

def get_name(doc_title, person_name_div, person_string, names_list, name_print_str, name_lookup_str,name_dict):
    names = person_name_div.find_all(name_lookup_str)
    for name in names:
        try:
            name_str = swap_name(name["reg"])
        except KeyError:
            name_str = ""
            for n in name:
                try:
                    name_str = name_str + n.text + " "
                except:
                    name_str = name_str + n + " "
        if name_str != "":
            person_string = person_string + f"{doc_title}'s {name_print_str} is {name_str}. "
            names_list.append(name_str.strip())
            name_dict[name_print_str] = name_str.strip().strip()
    return names_list, person_string

def bio_person_name(biography, standard_name, doc_title):
    names = []
    name_dict = {}
    # Person Names
    if re.sub(",", "", doc_title.strip().lower()) != re.sub(",", "", standard_name.strip().lower()) and "elder" not in doc_title and "younger" not in doc_title:
        person_string = f"{doc_title}'s full name is {standard_name}. "
    else:
        person_string = ""
    person_name_div = biography.find("PERSONNAME")
    
    if person_name_div:
        names, person_string = get_name(doc_title, person_name_div, person_string, names, "birth name", "BIRTHNAME",name_dict)
        names, person_string = get_name(doc_title, person_name_div, person_string, names, "married name", "MARRIED",name_dict)
        names, person_string = get_name(doc_title, person_name_div, person_string, names, "nickname", "NICKNAME",name_dict)
        names, person_string = get_name(doc_title, person_name_div, person_string, names, "pseudonym", "pseudonym",name_dict)
        names, person_string = get_name(doc_title, person_name_div, person_string, names, "religious name", "religiousname",name_dict)
        names, person_string = get_name(doc_title, person_name_div, person_string, names, "royal name", "royal",name_dict)
        names, person_string = get_name(doc_title, person_name_div, person_string, names, "self-constructed name", "selfconstructed",name_dict)
        names, person_string = get_name(doc_title, person_name_div, person_string, names, "indexed name", "indexed",name_dict)
        names, person_string = get_name(doc_title, person_name_div, person_string, names, "surname name", "SURNAME",name_dict)
        names, person_string = get_name(doc_title, person_name_div, person_string, names, "given name", "GIVEN",name_dict)

        person_name_div.find_parent().decompose()
        return name_dict


def parse_orlando_biography(biography, standard_name, doc_title, output_headings):
    '''
    if output_headings:
        out_file.write(f"\n\nBiography for {standard_name}\n\n")
    '''
    # parse personname tag then remove that section
    names = bio_person_name(biography, standard_name, doc_title)
    return names
    
def main():

    filename ="RDF-extraction-alberta-edits/data/places.csv"
    places_dict = {}
    with open(filename, 'r') as data:
        reader = csv.DictReader(data, fieldnames=['name', 'url'])
        for row in reader:
            places_dict[row['name']] = row['url']


    file_dict = utilities.parse_args(__file__, "Majority of biography related data")

    for f in file_dict:
        print(f)

    entry_num = 1
    uber_graph = utilities.create_graph()

    highest_triples = 0
    least_triples = 0
    smallest_person = None
    largest_person = None
    logger.info("Time started: " + utilities.get_current_time() + "\n")

    for filename in file_dict.keys():
        with open(filename) as f:
            soup = BeautifulSoup(f, 'lxml-xml')

        person_id = filename.split("/")[-1][:6]

        print(person_id)
        print(file_dict[filename])
        print("*" * 55)
        person = Biography(person_id, soup, cf.get_mapped_term("Gender", utilities.get_sex(soup)))

        names = bio_person_name(soup,person.name,person.name)

        facts = []
        if names:
            for name in names:
                if person.name and names[name]:
                    facts.append([person.name,name,names[name]])

        #print("[ FILE_ID ] is [", person.id, "]")
        #out_file.write(tab_join("FILE_ID", "is", person.id))
        #print("[ BIOGRAPHY_SUBJECT ] is [", person.name, "]")
        #out_file.write(tab_join("BIOGRAPHY_SUBJECT", "is", person.name))


       # facts.append([person.name, "type", "person"])
        if "woman" in person.gender.split("#")[1]:
            gender = "female"
        elif "man" in person.gender.split("#")[1]:
            gender = "male"
        else:
            gender = person.gender.split("#")[1]
        
        if person.name and gender:
            print("PERSON NAME IS ", person.name)
            facts.append([person.name, "gender", gender])
        # if person.wd_id:
        #     facts.append([person.name, "sameAs", person.wd_id])

        ## Cultural Forms
        cf.extract_cf_data(soup, person)
        for c in person.cf_list:
            property_cf = c.uri.split("#")[-1]
            if c.text.strip():
                if "nationality" in property_cf.lower():
                    property_cf = "nationality"
                    #facts.append([c.text, "type", "nationality"])
                elif "heritage" in property_cf.lower():
                    property_cf = "heritage"
                    #facts.append([c.text, "type", "national heritage"])
                elif "religion" in property_cf.lower():
                    property_cf = "religion"
                    #facts.append([c.text, "type", "religion"])
                elif "socialclass" in property_cf.lower():
                    property_cf = "social class"
                    #facts.append([c.text, "type", "social class"])
                elif "political" in property_cf.lower():
                    property_cf = "political affiliation"
                    #facts.append([c.text, "type", "political party"])
                elif "activist" in property_cf.lower():
                    property_cf = "political affiliation"
                    #facts.append([c.text, "type", "organization"])
                elif "memberof" in property_cf.lower():
                    property_cf = "employee or member of"
                    #facts.append([c.text, "type", "organization"])
                elif "race" in property_cf.lower():
                    property_cf = "race"
                    #facts.append([c.text, "type", "ethinic group"])
                elif "ethnicity" in property_cf.lower():
                    property_cf = "race"
                    #facts.append([c.text, "type", "ethinic group"])
                elif "sexuality" in property_cf.lower():
                    property_cf = "sexuality"
                    #facts.append([c.text, "type", "sexuality"])
                if person.name and c.text:
                    facts.append([person.name, property_cf, c.text])


        ## Locations
        # could get more specific location with l.predicate
        location.extract_location_data(soup, person)
        locations = person.location_list
        for l in locations:
            for ls in l.address.split(","):
                if person.name and ls.strip():
                    facts.append([person.name, "all person locations", ls.strip()])
                #facts.append([person.name, l.predicate, ls.strip()]) #to get more specific connection with location
                #facts.append([ls.strip(), "type", "location"])

            #facts.append([l.address.split(",")[0].strip(), "sameAs", l.value])

        ## Occupations
        occupation.extract_occupation_data(soup, person)
        for o in person.occupation_list:
            if person.name and o.job_tag:
                facts.append([person.name, "position held", o.job_tag])
            if o.job_tag:
                facts.append([o.job_tag, "type", "occupation"])
        # print(person.occupations)


        # Birth
        bdate, bplace, border = birthDeath.extract_birth_data(soup, person)
        for b in person.birth_list:
            if person.name and b.date:
                facts.append([person.name, "date of birth", b.date])
            for bs in bplace.split(","):
                if person.name and bs.strip():
                    facts.append([person.name, "place of birth", bs.strip()])
                    facts.append([person.name, "all person locations", bs.strip()])
                #facts.append([bs.strip(), "type", "location"])
            #facts.append([bplace.split(",")[0].strip(), "sameAs", b.place])
            if person.name and border:
                facts.append([person.name, "birth order", border])


        # Death
        death_info = birthDeath.extract_death_data(soup, person)
       
        death_date = death_info[0]
        death_place = death_info[1]
        death_place_url = death_info[2]
        death_cause = death_info[3]

        if death_date:
            facts.append([person.name, "date of death", death_date])
        if death_place:
            for ds in death_place.split(","):
                facts.append([person.name, "place of death", ds.strip()])
                facts.append([person.name, "all person locations", ds.strip()])
                #facts.append([ds.strip(), "type", "location"])

            # if death_place_url:
            #     facts.append([death_place.split(",")[0].strip(), "sameAs", death_place_url])

        if death_cause:
            facts.append([person.name, "cause of death", death_cause])


        #Cohabitants
        cohabs = lifeInfo.extract_cohabitants(soup, person)
        for c in cohabs:
            c = reorder_name(c)
            if person.name and c:
                facts.append([person.name, "lived with", c])
                facts.append([c, "lived with", person.name])
            #facts.append([c, "type", "person"])


        ## Family
        mem_list = lifeInfo.extract_family(soup, person)
        for m in mem_list:
            mem_name = reorder_name(m[0])
            rel = m[1]
            if mem_name:
                if rel == "MOTHER" or rel == "FATHER":
                    facts.append([person.name, "parent", mem_name])
                    facts.append([mem_name, "child", person.name])
                    if rel == "MOTHER":
                        facts.append([mem_name, "gender", "female"])
                    if rel == "FATHER":
                        facts.append([mem_name, "gender", "male"])
                elif rel == "CHILD" or rel == "SON" or rel == "DAUGHTER":
                    facts.append([person.name, "child", mem_name])
                    facts.append([mem_name, "parent", person.name])
                    if rel == "DAUGHTER":
                        facts.append([mem_name, "gender", "female"])
                    if rel == "SON":
                        facts.append([mem_name, "gender", "male"])
                elif rel == "SISTER" or rel == "BROTHER":
                    facts.append([person.name, "sibling", mem_name])
                    facts.append([mem_name, "sibling", person.name])
                    if rel == "SISTER":
                        facts.append([mem_name, "gender", "female"])
                    if rel == "BROTHER":
                        facts.append([mem_name, "gender", "male"])
                elif rel == "HUSBAND" or rel == "WIFE" or rel == "PARTNER":
                    facts.append([person.name, "spouse", mem_name])
                    facts.append([mem_name, "spouse", person.name])
                    if rel == "WIFE":
                        facts.append([mem_name, "gender", "female"])
                    if rel == "HUSBAND":
                        facts.append([mem_name, "gender", "male"])
                else:
                    facts.append([person.name, "family member", mem_name])
                    facts.append([mem_name, "family member", person.name])
                
                for j in m[2]:
                    facts.append([mem_name, "position held", j]) # occupation
                    #facts.append([j, "type", "occupation"])
                for s in m[3]:
                    facts.append([mem_name, "significant activity", s])
                #facts.append([mem_name, "type", "person"])



        # Friends & Associates
        friends = lifeInfo.extract_friends_associates(soup, person)  #get label not tag. remove the subject of the bio
        for friend in friends:
            friend = reorder_name(friend)
            if person.name and friend:
                facts.append([person.name, "social relationship", friend])
                facts.append([friend, "social relationship", person.name])
            #facts.append([friend, "type", "person"])


        # Intimate Relationships TODO remove relationships with oneself
        relationships = lifeInfo.extract_intimate_relationships(soup, person)
        for r in relationships:
            r = reorder_name(r)
            if r.lower().strip() != person.name.lower().strip():
                if person.name and r:
                    facts.append([person.name, "romantic relationship", r])
                    facts.append([r, "romantic relationship", person.name])
                    #facts.append([r, "type", "person"])


        # Children
        num_children = lifeInfo.extract_children(soup, person)
        if num_children:
            facts.append([person.name, "number of children", num_children])



        ## Education
        ## To add files with examples of this
        # print("\n\n\n")
        # print(person.education_list)
        # print("\n\n\n")


        ## NH might not need?
        #other_contexts.extract_other_contexts_data(soup, person)
        #other_contexts.extract_health_contexts_data(soup, person)
        #personname.extract_person_name(soup, person)


        ## Childlessnessperson.childless_list
        ## this isn't perfect because sometimes a marriage is childless and not a person
        lifeInfo.extract_childlessness(soup, person)
        #print(person.childless_list)
        if len(person.childless_list) > 0:
            facts.append([person.name, "number of children", "0"])


        #print(person.id)
        #print(person.name)
        #print(person.gender.split("#")[1])
        #print(person.wd_id)

        ##print(person.nationalities)
        # print(person.context_list)
        # print(person.event_list)
        # print(person.cf_list)
        # print(person.name_list)


        facts.sort()
        unique_facts = list(facts for facts,_ in itertools.groupby(facts))
        with open("RDF-extraction-alberta-edits/data/bio_temp/" + person_id + "_cwrc_output.tsv", "w") as out_file:
            out_file.write("subject\trelation\tobject\n")
            for fact in unique_facts:
                out_file.write(fact[0] + "\t" + fact[1] + "\t" + fact[2] +"\n")



#________________________________________________________________________________________________________________________________________________

    #     graph = person.to_graph()
    #     triple_count = len(graph)

    #     if triple_count > highest_triples:
    #         highest_triples = triple_count
    #         largest_person = filename
    #     if least_triples == 0 or triple_count < least_triples:
    #         least_triples = triple_count
    #         smallest_person = filename

    #     # triples to files
    #     temp_path = "extracted_triples/biography_turtle/" + person_id + "_biography.ttl"
    #     utilities.create_extracted_file(temp_path, person)

    #     uber_graph += graph
    #     entry_num += 1

    # temp_path = "extracted_triples/biography_triples.ttl"
    # create_extracted_uberfile(temp_path, uber_graph)

    # cf.log_mapping_fails()
    # logger.info(str(len(uber_graph)) + " total triples created")
    # logger.info(str(largest_person) + " produces the most triples(" + str(highest_triples) + ")")
    # logger.info(str(smallest_person) + " produces the least triples(" + str(least_triples) + ")")

    # logger.info("Time completed: " + utilities.get_current_time())


if __name__ == "__main__":
    main()
