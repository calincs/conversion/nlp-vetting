google_api_python_client==2.47.0
google_auth_oauthlib==0.5.1
gspread==5.3.2
oauth2client==4.1.3
pandas==1.4.2
protobuf==3.20.1
PyDrive==1.3.1
python-dotenv==0.20.0
requests==2.27.1
SPARQLWrapper==2.0.0
SQLAlchemy==1.4.35
