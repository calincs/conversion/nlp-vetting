# Diffbot Output Vetting 

This repository contains the workflow to vet the output from the Diffbot NLP API. 

This vetting is intended as a verification process for Diffbot’s performance on new datasets. It may also be used for portions of the output that are not reliable enough to put directly into ResearchSpace.


# Notebook

Download the notebook "diffbot-output-vetting-final.ipynb" either on your local system or the online server. This notebook has an elaborate description of the vetting workflow.


## How to open a Jupyter Notebook
### Option 1. Opening from your terminal

Open your system's terminal and type the command ``` jupyter notebook ``` . 

A browser window should immediately pop up with the Jupyter Notebook interface. 
From there, you can navigate to the correct directory with the Jupyter Notebook. 

For more info go to this link:
https://www.codecademy.com/article/how-to-use-jupyter-notebooks

### Option 2. Anaconda

www.anaconda.com/products/distribution

Anaconda is a platform that allows users to easily access and launch Python programs, including Jupyter Notebooks. After downloading the app from the link, you can select and launch Jupyter Notebook from the home menu.

#### Once you have opened the notebook, read through it carefully and run all code cells INDIVIDUALLY.
