'''
This program interacts with google sheets, and creates 4 new tabs based on whether records are entities/facts and accurate/inaccurate. 
For the purpose of easy diffbot output vetting.
'''
import gspread
from oauth2client.service_account import ServiceAccountCredentials
from pathlib import Path
from pydrive.auth import GoogleAuth
import pandas as pd
import sys
from format_sheet import format_after
import json
import os
from dotenv import load_dotenv

load_dotenv()
def get_sheets_client(credentials):
	# use creds to create a client to interact with the Google Drive API
	sheets_scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
	sheets_creds = ServiceAccountCredentials.from_json_keyfile_dict(credentials, sheets_scope)
	sheets_client = gspread.authorize(sheets_creds)
	return sheets_client

def accuracy_extract(sheet,tab,num_worksheet,type_extract,col_num):
    # fetch all records
    records = tab.get_all_records()
    # fetch rows based on the accurate value
    acc = []
    inacc = []
    if type_extract == 'entities':
        for row in records:
            if row['accurate_entity'].lower() == 'yes' and row['accurate_type'].lower() == 'yes' and row['accurate_mentions'].lower() == 'yes':
                acc.append(row)
            else:
                inacc.append(row)
    else:
        for row in records:
            if row['accurate_fact'].lower() == 'yes' or (row['accurate_subject'].lower() == 'yes' and row['accurate_relation'].lower() == 'yes' and row['accurate_object'].lower() == 'yes'):
                acc.append(row)
            else:
                inacc.append(row)

    # adding new worksheets, one for accurate and another for inaccurate type
    
    sheet.add_worksheet(rows=len(acc), cols=col_num, title = 'accurate_{}'.format(type_extract))
    sheet.add_worksheet(rows=len(inacc), cols=col_num, title = 'inaccurate_{}'.format(type_extract))
    sheet_acc = sheet.get_worksheet(num_worksheet)
    sheet_inacc = sheet.get_worksheet(num_worksheet+1)
    # creating data frame for updating worksheets
    records_acc = pd.DataFrame.from_dict(acc)
    records_inacc = pd.DataFrame.from_dict(inacc)
    # preparing rows for worksheet
    acc_values = records_acc.values.tolist()
    # adding row with column names
    acc_values.insert(0,records_acc.keys().tolist())
    sheet_acc.insert_rows(acc_values)
    # same process for inaccurate values
    inacc_values = records_inacc.values.tolist()
    inacc_values.insert(0,records_inacc.keys().tolist())
    sheet_inacc.insert_rows(inacc_values) 

def main():
    # authenticate to access google sheets
    token_file_str = os.getenv("TOKEN_FILE").replace('\\','')
    token_file = json.loads(token_file_str,strict=False)

    sheets_client = get_sheets_client(token_file)
    # open spreadsheet
    sheet = sheets_client.open(sys.argv[1])
    entity_tab = sheet.worksheet('entities')
    fact_tab = sheet.worksheet('facts')
    # checking if accuracy tabs already exist
    sheet_names = [s.title for s in sheet.worksheets()]
    if 'accurate_entities' in sheet_names:
        for s in sheet.worksheets():
            if 'accurate' in s.title or 'inaccurate' in s.title:
                sheet.del_worksheet(s)
        
    # for entities
    accuracy_extract(sheet,entity_tab,2,'entities',15)
    # for facts
    accuracy_extract(sheet,fact_tab,4,'facts',15)
    # formatting the extracted worksheet by calling function from format_sheet.py
    format_after(sheet)
      
if __name__ == "__main__":
    main()


