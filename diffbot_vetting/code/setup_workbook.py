from cmath import nan
import io
import shutil
import gspread
import sys
from pathlib import Path
import copy
import json
import os
import time
from pytz import utc
import requests
import csv
import pandas as pd
from sqlalchemy import column
from format_sheet import format_before
from googleapiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials
import datetime
from googleapiclient.discovery import build
from googleapiclient.http import MediaIoBaseDownload
from dotenv import load_dotenv
from collections import Counter, defaultdict
from apiclient.http import MediaFileUpload
from SPARQLWrapper import SPARQLWrapper, JSON

load_dotenv()
# read input
input_file = sys.argv[1]   # The text file
input_type = sys.argv[2]   # input type, single/split
entity_name = sys.argv[3]  # name of entity


# define output files
source_file_link = None
output_file_entity = f"{entity_name}/{entity_name}_entity.txt"
output_file_facts = f"{entity_name}/{entity_name}_facts.txt"

SCOPES = ['https://www.googleapis.com/auth/drive.metadata.readonly', 'https://www.googleapis.com/auth/drive']


def get_sheets_client(credentials):
    # use creds to create a client to interact with the Google Drive API
    sheets_scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
    sheets_creds = ServiceAccountCredentials.from_json_keyfile_dict(credentials, sheets_scope)
    sheets_client = gspread.authorize(sheets_creds)
    return sheets_client

# this function divides the 'humanReadable' column into three cols and adds 3 new columns 
def divide_fact_col(records,old_acc_facts):
    dups = []
    for row in records:
        fact = row['humanReadable']
        fact = fact.replace(']','[')
        fact = fact.split('[')
        fact = remove_values_from_list(fact,'')
        for i in range(0,len(fact)):
            fact[i] = fact[i].strip()
        
        # check duplicates
        triple = fact[0] + fact[1] + fact[2]
        if triple in dups:
            row.clear()
            continue
        dups.append(triple)

        del row['humanReadable']
        new_row = copy.deepcopy(row)
        row.clear()
        row['subject'] = fact[0]
        row['relation'] = fact[1]
        row['object'] = fact[2]
        if 'qualifiers' not in row.keys():
            row['qualifiers'] = ''
        row['mentions'] = ''
        row.update(new_row)
        
        if old_acc_facts and [row['subject'],row['relation'],row['object']] in old_acc_facts:
            row['accurate_subject'] = 'yes'
            row['accurate_relation'] = 'yes'
            row['accurate_object'] = 'yes'
            row['accurate_fact'] = 'yes'
            old_acc_facts.remove([row['subject'],row['relation'],row['object']])
        else:
            row['accurate_subject'] = ''
            row['accurate_relation'] = ''
            row['accurate_object'] = ''
            row['accurate_fact'] = ''
    
    if old_acc_facts:
        with open('missed_facts.tsv','w') as f:
            writer = csv.writer(f,delimiter='\t')
            writer.writerows(old_acc_facts)
        
    return records  

# this function adds 4 new columns in the entity tab
def add_entities_acc(records,old_acc_entities):
    for row in records:
        row['wikidataInfo'] = ''
        if row['allUris'] and row['allUris'] != '[]':
            name,desc = wikidata_query(row['allUris'])
            row['wikidataInfo'] =  name +'\n'+desc
        
        if 'location' not in row.keys():
            row['location'] = ''
        row['replace_w_url'] = ''
        if old_acc_entities and row['name'] in old_acc_entities:
            row['accurate_entity'] = 'yes'
            row['accurate_type'] = 'yes'
            row['accurate_mentions'] = 'yes'
            old_acc_entities.remove(row['name'])
        else:
            row['accurate_entity'] = ''
            row['accurate_type'] = ''
            row['accurate_mentions'] = ''  

        if old_acc_entities:
            with open('missed_entities.tsv','w') as f:
                writer = csv.writer(f,delimiter='\t')
                writer.writerows(old_acc_entities)  
    return records

# this function removes a value from a list
def remove_values_from_list(the_list, val):
   return [value for value in the_list if value != val]

# this function convert input json data to tsv files
def convert_input(json_data):
    if "entities" in json_data:
        df = pd.DataFrame.from_dict(json_data["entities"])
        #df = pd.DataFrame.from_dict(sorted(json_data["entities"], key = lambda ent: ent["salience"], reverse=True))
        df.to_csv(output_file_entity, sep='\t', index=False)

    if "facts" in json_data:
        df = pd.DataFrame.from_dict(json_data["facts"])
        df.to_csv(output_file_facts, sep='\t', index=False)

# this function extracts the sentence containing the mentioned offsets
def find_evidence(start,end,content):
    punctuations = '\n!.?'
    # find starting position
    found = False
    start = start + 1
    while not found:
        try:
            char = content[start]
        except:
            begin = start + 1
            break
        if char in punctuations:
            begin = start + 1
            found = True
        else:
            start -= 1

    # find ending position
    found = False
    #end = end + 2
    while not found:
        try:
            char = content[end]
        except:
            ending = end - 1
            break
        if char in punctuations:
            ending = end + 1
            found = True
        else:
            end += 1
    sentence = content[begin:ending]
    return sentence

# this function updates each row in the csv file with the evidence for entities
def extract_entities(row, file_content):
        if 'mentions' in row.keys():
            mentions = row['mentions']
        else:
            return
        # convert string to list
        mentions = list(eval(mentions))
        # find evidence
        cell = []
        names = []
        for dic in mentions:
            if isinstance(dic, str): 
                continue
            start = int(dic['beginOffset']) 
            end = int(dic['endOffset'])
            evidence = find_evidence(start,end,file_content)
            
            # add names to list
            if dic['text'] not in names:
                names.append(dic['text'])
            
            # check for duplicates
            if evidence not in ''.join(cell):
                cell.append(evidence)
        
        # format evidence
        
        cell = ''.join(cell).strip("['").strip("']")
        split_names = []
        for name in names:
            split_names.extend(name.split())
        
        split_names = list(dict.fromkeys(split_names))
        for name in split_names:
            cell = cell.replace(' '+name+' ','【'+name+'】')
            cell = cell.replace(' '+name+'.','【'+name+'】.')
            cell = cell.replace(' '+name+',','【'+name+'】,')
            cell = cell.replace(' '+name+'\n','【'+name+'】\n')
        # add new line to cell
        cell = cell.replace('.','.\n')

        # add evidence to original records
        
        row['evidence'] = cell.strip()
        
 
        # extract type
        entity_type = ''
        types_list = list(eval(row['allTypes']))
        for dic in types_list:
            if isinstance(dic, str):
                continue
            entity_type += dic['name'] + ', '
        #print("TYPE:",entity_type.strip(', '))
        row['allTypes'] = entity_type.strip(', ')
        
        # remove uri from list
        if row['allUris']:
            row['allUris'] = row['allUris'].strip("['").strip("']")
        
        return row

# this function updates each row in the csv file with the evidence for facts
def extract_facts(row):
        # get mention column
        if 'evidence' in row.keys():
            mentions = row['evidence']
        else:
            return
        # convert string to list
        mentions_str = mentions
        mentions = list(eval(mentions))
        passage = ''
        for dic in mentions:
            if 'passage' in dic.keys():
                passage = dic['passage']
                entity_text = dic['entityMentions'][0]['text']
                value_text = dic['valueMentions'][0]['text']
                passage = passage.replace(' '+entity_text+' ','【'+entity_text+'】')
                passage = passage.replace(' '+value_text+' ','【'+value_text+'】')
                passage = passage.replace(' '+entity_text+'.','【'+entity_text+'】.')
                passage = passage.replace(' '+value_text+'.','【'+value_text+'】.')
                passage = passage.replace(' '+entity_text+',','【'+entity_text+'】,')
                passage = passage.replace(' '+value_text+',','【'+value_text+'】,')
                passage = passage.replace(' '+entity_text+'\n','【'+entity_text+'】\n')
                passage = passage.replace(' '+value_text+'\n','【'+value_text+'】\n')
        # replace mentions with passage text
        row['mentions'] = mentions_str
        row['evidence'] = str(passage)
        return row

# This function handles getting a list of past vetted results
def handle_past_vetting(recent_folder_id,drive_service,token_file):
    # query to list all files in a folder
    results = drive_service.files().list(q = "'{}' in parents".format(recent_folder_id), pageSize=10, fields="nextPageToken, files(id, name)").execute()
    items = results.get('files', [])
    output_folder_id = ''
    metadata_file_id = ''
    # checking if 
    for item in items:
        if 'output' in item['name']:
            output_folder_id = item['id']
        if 'metadata' in item['name']:
            metadata_file_id = item['id']
    
    # downloading metadata file from the past vetted folder
    if metadata_file_id:
        request = drive_service.files().get_media(fileId=metadata_file_id)
        fh = io.BytesIO()
        downloader = MediaIoBaseDownload(fh, request)
        done = False
        while done is False:
            status, done = downloader.next_chunk()
            print("Download %d%%" % int(status.progress() * 100))
        fh.seek(0)
        with open('{}_metadata.json'.format(entity_name), 'wb') as f:
            shutil.copyfileobj(fh, f, length=131072)
    # opening accurate_entities file and getting all records
    
    sheets_client = get_sheets_client(token_file)
    
    results = drive_service.files().list(q = "'{}' in parents".format(output_folder_id), pageSize=10, fields="nextPageToken, files(id, name)").execute()
    items = results.get('files', [])
    names = []
    for item in items:
        names.append(item['name'])

    old_entities = []
    if 'accurate_entities.tsv' in names:
        sheet = sheets_client.open('accurate_entities.tsv',folder_id = output_folder_id)
        tab = sheet.worksheet('accurate_entities')
        acc_entities = tab.get_all_records()
        for entity in acc_entities:
            old_entities.append(entity['name'])
    
    # opening accurate_facts file and getting all records
    old_facts = []
    if 'accurate_facts.tsv' in names:
        sheet = sheets_client.open('accurate_facts.tsv',folder_id = output_folder_id)
        tab = sheet.worksheet('accurate_facts')
        acc_facts = tab.get_all_records()
        for entity in acc_facts:
            old_facts.append([entity['subject'],entity['relation'],entity['object']])
    
    return [old_entities,old_facts]

def upload_diffbot_results(drive_service,new_folder_id):
    # creating a new folder to store diffbot results
    file_metadata = {
    'name': 'Diffbot_results',
    'parents': [new_folder_id],
    'mimeType': 'application/vnd.google-apps.folder'
    }
    file = drive_service.files().create(body=file_metadata,
                                        fields='id').execute()
    res_folder_id = file.get('id')

    # creating a new folder to store input files
    file_metadata_input = {
    'name': 'Input_files',
    'parents': [new_folder_id],
    'mimeType': 'application/vnd.google-apps.folder'
    }
    input_file = drive_service.files().create(body=file_metadata_input,
                                        fields='id').execute()
    res_folder_input_id = input_file.get('id')

    for file in os.listdir('diffbot_results'):
        file_metadata = {
            'name': file,
            'parents': [res_folder_id]
            }
        media = MediaFileUpload('diffbot_results/'+file,
                            mimetype='application/json',
                                    resumable=True)
    
        drive_service.files().create(body=file_metadata,
                                        media_body=media,
                                        fields='id').execute()

    for file in os.listdir(entity_name+"/input"):
        file_metadata = {
            'name': file,
            'parents': [res_folder_input_id]
            }
        media = MediaFileUpload(entity_name+"/input/"+file,
                            mimetype='text/plain',
                                    resumable=True)
    
        drive_service.files().create(body=file_metadata,
                                        media_body=media,
                                        fields='id').execute()

def wikidata_query(wikidata_url):
    wikidata_url = wikidata_url.lstrip('[\'')
    wikidata_url = wikidata_url.rstrip('\']')
    wikidata_id = wikidata_url.rsplit('/', 1)[-1]
    sparql = SPARQLWrapper("https://query.wikidata.org/sparql")
    sparql.setQuery("""
    SELECT ?item ?itemLabel ?itemDesc WHERE {
    VALUES ?item { wd:%s }
    SERVICE wikibase:label { bd:serviceParam wikibase:language "en". 
    ?item rdfs:label ?itemLabel .
    wd:%s schema:description ?itemDesc .}
    }
    """% (wikidata_id,wikidata_id)) 
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    results_df = pd.json_normalize(results['results']['bindings'])
    results_dict = results_df.to_dict('records')
    name = results_dict[0]['itemLabel.value']
    if 'itemDesc.value' in results_dict[0].keys():
        desc = results_dict[0]['itemDesc.value']
    else:
        desc = ''
    return name,desc


def main():
    # authenticate to access google sheets
    creds = None
    
    token_file_str = os.getenv("TOKEN_FILE").replace('\\','')
    
    token_file = json.loads(token_file_str,strict=False)

    creds = ServiceAccountCredentials.from_json_keyfile_dict(token_file, SCOPES)
    
    # authenticate to access google drive
    drive_service = build('drive', 'v3', credentials=creds)
    folder_id = '1Wbv0NnveBLsKl95V3ryqIPxXLho8PUzs'
    if os.path.exists('diffbot_results'):
        shutil.rmtree('diffbot_results')
    os.mkdir('diffbot_results')
    # handle past vetting results
    try:
        past_vet_folder = sys.argv[4]
        if past_vet_folder == 'None':
            past_vet_folder = None
    except:
        past_vet_folder=None

    old_acc_entities = []
    old_acc_facts = []
    if past_vet_folder:
        results = drive_service.files().list(q = "'1Wbv0NnveBLsKl95V3ryqIPxXLho8PUzs' in parents", pageSize=10, fields="nextPageToken, files(id, name,modifiedTime)").execute()
        items = results.get('files', [])
        found = False
        for item in items:
            if item['name'] == past_vet_folder:
                old_acc_entities, old_acc_facts = handle_past_vetting(item['id'],drive_service,token_file)
                found = True
        if not found:
            print("No folder with that name was found. Continuing without including past results.")
        
    # create entity folder locally
    # !!might not need this!!
    if os.path.exists(entity_name):
        shutil.rmtree(entity_name)
    
    os.mkdir(entity_name)
    os.mkdir('{}/input'.format(entity_name))
    
    folder_name = entity_name+'_'+ datetime.datetime.utcnow().strftime("%m/%d/%Y,%H:%M:%S")
    # create entity folder on google drive
    file_metadata = {
    'name': folder_name,
    'parents': [folder_id],
    'mimeType': 'application/vnd.google-apps.folder'
    }
    file = drive_service.files().create(body=file_metadata,
                                        fields='id').execute()
    new_folder_id = file.get('id')
    folder_dets_json = {"folder_id":new_folder_id,"folder_name":folder_name}
    # saving reference to folder_id
    with open('folder_dets.json','w') as f:
        json.dump(folder_dets_json,f)
    
    # saving reference to sheet client
    sheets_client = get_sheets_client(token_file)

    # **handling input**

    # user inputs split files
    if input_type.lower() == 'split':

        # save reference to input and output dir
        input_dir = input_file
        output_dir = os.path.join(entity_name,'input')

        # move files to input folder
        # no need for this i believe
        for file in os.listdir(input_dir):
            if file == 'input':
                continue
            source = os.path.join(input_dir,file)
            dest = os.path.join(output_dir,file)
            shutil.copy2(source,dest)

        # concatanate all txt files in a file
        merged_txt = entity_name +'.txt'
        with open(merged_txt, 'w') as outfile:
            for filename in os.listdir(output_dir):
                filename = os.path.join(output_dir,filename)
                with open(filename, encoding="utf-8", errors='ignore') as infile:
                    outfile.write(infile.read())
                    outfile.write('\n\n')

    # user inputs single text
    elif input_type.lower() == 'single':
        # define output dir
        output_dir = os.path.join(entity_name,'input')
        shutil.copy2(input_file,os.path.join(output_dir,input_file))
    else:
        shutil.copy2(sys.argv[5],os.path.join(entity_name,'input'))
        shutil.copy2(input_file,os.path.join(entity_name,'input'))
    # set up varaibles for diffbot request
    if input_type != 'json':
        TOKEN = os.getenv("DIFFBOT_TOKEN")
        FIELDS = "entities,facts"
        HOST = "nl.diffbot.com"
        PORT = "80"

        def get_request(payload):
            start = time.time()
            res = requests.post("https://{}/v1/?fields={}&token={}".format(HOST, FIELDS, TOKEN), json=payload)
            ret = None
            try:
                ret = res.json()
            except:
                print("Bad response: " + res.text)
                print(res.status_code)
                print(res.headers)
            end = time.time()
            return ret, end - start
    


    # defining spreadsheet
    sheet = sheets_client.create(entity_name, folder_id= new_folder_id)
    sheet.add_worksheet(rows=500, cols=15, title = 'entities')
    sheet.add_worksheet(rows=500, cols=15, title = 'facts')
    sheet.del_worksheet(sheet.worksheet('Sheet1'))
    entity_tab = sheet.worksheet('entities')
    fact_tab = sheet.worksheet('facts')
    input_dir = '{}/input'.format(entity_name)

    # list of dictionaries
    all_ent_records = []
    all_fact_records = []
    json_res_count = 1
    # iterate through files in dir
    for file in sorted(os.listdir(input_dir)):
        if input_type == 'json':
            if file.endswith('.txt'):
                continue
            with open(input_file,'r') as f:
                fcontent = f.read()
        else:
            with open(os.path.join(input_dir,file),'r',encoding='utf-8') as f:
                fcontent = f.read()
        
        if not fcontent:
            continue
        
        if input_type != 'json':
            res, timer = get_request({
                "content": fcontent,
                "lang": "en",
                "format": "plain text with title",
            })
            if json_res_count == 1:
                file_name_json = entity_name+"_res.json"
            else:
                file_name_json = entity_name+"_res_{}.json".format(json_res_count)
            json_res_count = json_res_count + 1
            with open('diffbot_results/'+file_name_json,'w') as f:
                json.dump(res,f,indent=4)
                
        else:
            with open(sys.argv[5],'r') as json_file:
                res = json.loads(json_file.read())
            shutil.copy(file,'diffbot_results')

        # generating entities and facts tsv from json file

        convert_input(res)

        # check if entities exist
        try:
            entities_df = pd.read_csv(output_file_entity,sep = '\t')
            entities = True
        except:
            entities = False
            
        try:
            facts = True
            facts_df = pd.read_csv(output_file_facts,sep = '\t')
        except:
            facts = False
        
        # check if any facts exist
        if facts:
            facts_df = facts_df.fillna('')

            # dividing facts column and creating facts list of dictionaries
            facts_list = facts_df.to_dict('records')
            facts_list_dic = divide_fact_col(facts_list,old_acc_facts)
            
            # find mentions in facts tab, add to data frame
            for row in facts_list_dic:
                row = extract_facts(row)

        # creating entity list of dictionaries
        if entities:

            entities_df = entities_df.fillna('')
            entities_list_dic = entities_df.to_dict('records')
            entities_list_dic = add_entities_acc(entities_list_dic,old_acc_entities)

            # find mentions in entites tab, add to dataframe
            for row in entities_list_dic:
                row = extract_entities(row, fcontent)
            # add entities record to list
            for record in entities_list_dic:
                all_ent_records.append(record)

        
        if facts:
            for fact in facts_list_dic:
                if fact:
                    # making sure there are no duplicates when files are split
                    sub = fact['subject']
                    rel = fact['relation']
                    obj = fact['object']
                    to_add = True
                    for record in all_fact_records:
                        if record['subject'] == sub and record['relation'] == rel and record['object'] == obj:
                            to_add = False
                    if to_add:
                        fact['input_text_name'] = str(file)
                        all_fact_records.append(fact)
    
    if all_ent_records:
        if input_type == 'split':
            ent_values = merge_mentions(all_ent_records)
        else:
                # add list of dicts to data frame
            entities_df =pd.DataFrame.from_dict(all_ent_records)
            ent_values = entities_df.values.tolist()
        
        entity_tab.insert_rows(ent_values)
    
    upload_diffbot_results(drive_service,new_folder_id)
    
    if all_fact_records:
        facts_df =pd.DataFrame.from_dict(all_fact_records)
        fact_values = facts_df.values.tolist()

        fact_tab.insert_rows(fact_values)

    format_before(sheet)
    shutil.rmtree('diffbot_results')
    


def merge_mentions(records):
    
    count = dict(Counter(d['name'] for d in records))
    dup_values = []
    for row in count:
        if count[row] > 1:
            dup_values.append(row)
    
    d = defaultdict(list)
    for dup in dup_values:
        for i in records:
            if i['name'] == dup and i['evidence'] not in d[i['name']]:
                d[i['name']].append(i['evidence'])
    
    records_list = []
    for entity in dict(d):
        first_one = True
        for i in range(0,len(records)):
            # i is index
            record_dict = records[i]
            if record_dict['name'] == entity:
                if first_one:
                    records[i]['evidence'] = ''.join(d[entity])
                    first_one = False
                    records_list.append(record_dict)
                
            else:
                if record_dict not in records_list and record_dict['name'] not in dup_values:
                    records_list.append(record_dict)
    
    
    entities_df =pd.DataFrame.from_dict(records_list)
    headers = entities_df.keys().to_list()
    ent_values = entities_df.values.tolist()
    return ent_values
    
    
if __name__ == "__main__":
    main()


