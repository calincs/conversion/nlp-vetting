import csv
import sys
import pandas as pd

def remove_values_from_list(the_list, val):
   return [value for value in the_list if value != val]
   
def main():
    file_name = sys.argv[1]
    facts = []
    with open(file_name,'r') as f:
        content = csv.reader(f, delimiter="\t")
        for line in content:
            fact_str = line[0]
            fact = fact_str.replace(']','[')
            fact = fact.split('[')
            fact = remove_values_from_list(fact,'')
            for i in range(0,len(fact)):
                fact[i] = fact[i].strip()
            facts.append(fact)

    with open('accurate_facts','w') as f:
        writer = csv.writer(f,delimiter='\t')
        facts.insert(0,['subject','relation','object'])   
        writer.writerows(facts)
    

main()