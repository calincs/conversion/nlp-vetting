# this function changhes the location of columns
def bold_headers(sheet,sheetID,startColInd,endColInd):
    body = {
    "requests": [
        {
            "repeatCell": {
                "range": {
                    "sheetId": sheetID,
                    "endRowIndex": 1,
                    "startColumnIndex": startColInd,
                    "endColumnIndex": endColInd
                    
                },
                "fields": "userEnteredFormat/textFormat",
                "cell": {
                    "userEnteredFormat": {
                        "textFormat": {
                            "bold": True,
                            "foregroundColor": {
                                "blue": 1.0
                            },
                            "fontSize": 10
                        }
                    }
                }
            }
        }
    ],
    "responseRanges": [
        "1:2"
    ],
    "includeSpreadsheetInResponse": True
    }
    sheet.batch_update(body)

def move_col(sheet,sheetId,startIndex,endIndex,destIndex):
    body = {
    "requests": [
        {
        "moveDimension": {
            "source": {
            "sheetId": sheetId,
            "dimension": "COLUMNS",
            "startIndex": startIndex,
            "endIndex": endIndex
            },
            "destinationIndex": destIndex
        }
        }
    ]}
    sheet.batch_update(body)

# this function hides columns 
def hide_col(sheet,sheetId,startIndex,endIndex):
    body = {
    "requests": [
        {
            "updateDimensionProperties": {
                "properties": {
                    "hiddenByUser": True
                },
                "range": {
                    "sheetId": sheetId,
                    "dimension": "COLUMNS",
                    "startIndex": startIndex,
                    "endIndex": endIndex
                },
                "fields": "hiddenByUser"
            }
        }
    ]
    }
    sheet.batch_update(body)

# this function automatically adjusts column size
def adjust_col(sheet,sheetId,startIndex,endIndex):
    body = {
        "requests": [
            {
                "autoResizeDimensions": {
                    'dimensions': {
                        'sheetId': sheetId, 
                        'dimension': 'COLUMNS', 
                        'startIndex': startIndex, 
                        'endIndex': endIndex
                    }
                }   
            }
        ]
    }
    sheet.batch_update(body)

def adjust_row(sheet,sheetID,no_ent_rows):
    body = {"requests":[{
      "updateDimensionProperties": {
        "range": {
          "sheetId": sheetID,
          "dimension": "ROWS",
          "startIndex": 1,
          "endIndex": (no_ent_rows+6)
        },
        "properties": {
          "pixelSize": 80
        },
        "fields": "pixelSize"
      }
    }]}
    sheet.batch_update(body)

# this function wraps text and freezes the first row and column of a worksheet
def wrap_freeze(entity_tab,fact_tab):
    entity_tab.freeze(rows=None,cols=1)
    fact_tab.freeze(rows=None,cols=3)
    # for wrap text
    entity_tab.format("A:N", {"wrapStrategy": "WRAP"})
    fact_tab.format("A:L", {"wrapStrategy": "WRAP"})
    # freeze rows and columns
    entity_tab.freeze(rows=1,cols=None)
    fact_tab.freeze(rows=1,cols=None)

# this function calls on other formatting functions before the accurate/inaccurate tabs are extracted
def format_before(sheet):
    entity_tab = sheet.worksheet('entities')
    fact_tab = sheet.worksheet('facts')
    entity_tab_id = entity_tab.id
    fact_tab_id = fact_tab.id
    
    # add headers
    entity_header = ['name','diffbotUri','confidence','salience','isCustom','allUris','allTypes','mentions','location','wikidata_info','replace_w_url','accurate_entity','accurate_type','accurate_mentions','evidence']
    fact_header = ['subject','relation','object','qualifiers','mentions','entity','property','values','confidence','evidence','accurate_subject','accurate_relation','accurate_object','accurate_fact','input_text_name']

    #insert headers
    entity_tab.insert_row(entity_header,index=1)
    fact_tab.insert_row(fact_header,index=1)
    no_ent_rows= len(entity_tab.get_all_records())
    hide_col(sheet,entity_tab_id,1,2)
    hide_col(sheet,entity_tab_id,3,5)
    hide_col(sheet,entity_tab_id,7,9)
    hide_col(sheet,fact_tab_id,3,8)
    bold_headers(sheet,entity_tab_id,10,14)
    bold_headers(sheet,fact_tab_id,10,14)
    adjust_col(sheet,entity_tab_id,0,15)
    adjust_col(sheet,fact_tab_id,0,14)
    adjust_row(sheet,entity_tab_id,no_ent_rows)
    move_col(sheet,entity_tab_id,5,6,9)
    move_col(sheet,entity_tab_id,5,6,11)

    wrap_freeze(entity_tab,fact_tab)
    

# function that calls on other formatting functions after the accurate/inaccurate tabs are extracted
def format_after(sheet):
    ac_entity_tab = sheet.worksheet('accurate_entities')
    ac_fact_tab = sheet.worksheet('accurate_facts')
    inac_entity_tab = sheet.worksheet('inaccurate_entities')
    inac_fact_tab = sheet.worksheet('inaccurate_facts')
    # getting ids of spreadsheets
    ac_entity_tab_id = ac_entity_tab.id
    ac_fact_tab_id = ac_fact_tab.id
    inac_entity_tab_id = inac_entity_tab.id
    inac_fact_tab_id = inac_fact_tab.id

    hide_col(sheet,ac_entity_tab_id,1,2)
    hide_col(sheet,ac_entity_tab_id,3,7)
    #hide_col(sheet,ac_entity_tab_id,7,8)
    #hide_col(sheet,ac_entity_tab_id,10,11)
    hide_col(sheet,ac_fact_tab_id,3,8)
    adjust_col(sheet,ac_entity_tab_id,0,14)
    adjust_col(sheet,ac_fact_tab_id,0,12)
    #move_col(sheet,ac_entity_tab_id,5,6,9)
    #move_col(sheet,ac_entity_tab_id,5,6,11)
    wrap_freeze(ac_entity_tab,ac_fact_tab)    
   
    hide_col(sheet,inac_entity_tab_id,1,2)
    hide_col(sheet,inac_entity_tab_id,3,7)
    #hide_col(sheet,inac_entity_tab_id,7,8)
    #hide_col(sheet,inac_entity_tab_id,10,11)
    hide_col(sheet,inac_fact_tab_id,3,8)
    adjust_col(sheet,inac_entity_tab_id,0,14)
    adjust_col(sheet,inac_fact_tab_id,0,12)
    #move_col(sheet,inac_entity_tab_id,5,6,9)
    #move_col(sheet,inac_entity_tab_id,5,6,11)
    wrap_freeze(inac_entity_tab,inac_fact_tab)


  

  
    