import os
import platform
import datetime
#from create_sheet import file_name,input_file,source_file, source_file_link
import json
import sys
import shutil
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from oauth2client.service_account import ServiceAccountCredentials
from apiclient.http import MediaFileUpload
from dotenv import load_dotenv

load_dotenv()
file_name = sys.argv[1]
SCOPES = ['https://www.googleapis.com/auth/drive.metadata.readonly', 'https://www.googleapis.com/auth/drive']
# this function gets vetting details 
def vetting_details():
    author = sys.argv[2]
    current_time = datetime.datetime.now()
    return {'author':author,'current_time':current_time}
# this function gets the creation date of files
def output_creation_date(path_to_file):
    """
    Try to get the date that a file was created, falling back to when it was
    last modified if that isn't possible.
    See http://stackoverflow.com/a/39501288/1709587 for explanation.
    """
    if platform.system() == 'Windows':
        return os.path.getctime(path_to_file)
    else:
        stat = os.stat(path_to_file)
        try:
            return stat.st_birthtime
        except AttributeError:
            # We're probably on Linux. No easy way to get creation dates here,
            # so we'll settle for when its content was last modified.
            return stat.st_mtime
# this function gets stats notes from the user
def stat_notes():
    notes = sys.argv[3]
    return notes
        
def main():
    json_dict = {}
    # checking if file already exists, only need to append new data then. Otherwise create new file with data.
    if not os.path.isfile('{0}_metadata.json'.format(file_name)):
        output_date_created = None
        if sys.argv[5] != 'None':
            output_date_created = sys.argv[5]
        else:
            output_date_created = datetime.datetime.fromtimestamp(output_creation_date(file_name+"/"+file_name+'_entity.txt'))
            
        json_dict['output_file_details'] = {'file_name':'{0}_res.json'.format(file_name),'date_created':str(output_date_created)}
        try:
            source_file_link = sys.argv[6]
        except:
            source_file_link = None
        json_dict['source_file_details'] = {'file_name':sys.argv[4],'source_file_link':source_file_link}
        json_dict['vetting_details'] = []
    else:
        with open('{0}_metadata.json'.format(file_name),'r') as metadata_file:
            json_dict = json.load(metadata_file)
    
    # getting details by calling on previosuly defined functions
    vet_details = vetting_details()
    notes = stat_notes()
    json_dict['vetting_details'].append({'author':vet_details['author'],'vet_datetime':str(vet_details['current_time']),'notes':notes})
    file_path = '{0}_metadata.json'.format(file_name)
    with open(file_path,'w') as metadata_file:
        json.dump(json_dict,metadata_file,indent=4)

    # uploading metadata file to google drive
    
    folder_dets = {}
    with open('folder_dets.json','r') as f:
        folder_dets = json.load(f)
    folder_id = folder_dets['folder_id']
    creds = None
    # hide token
    token_file_str = os.getenv("TOKEN_FILE").replace('\\','')
    token_file = json.loads(token_file_str,strict=False)

    creds = ServiceAccountCredentials.from_json_keyfile_dict(token_file, SCOPES)
    
    drive_service = build('drive', 'v3', credentials=creds)
    
    file_metadata = {
            'name': file_name+"_metadata.json",
            'parents': [folder_id]
            }
    media = MediaFileUpload(file_path,
                            mimetype='application/json',
                                    resumable=True)
    
    drive_service.files().create(body=file_metadata,
                                        media_body=media,
                                        fields='id').execute()
    
    # removing unnecessary files from users system
    os.remove(file_path)
    #os.remove('folder_id.txt')
    shutil.rmtree(sys.argv[1])
   
if __name__ == "__main__":
    main()

