from __future__ import print_function
import gspread
from apiclient import discovery
import pandas as pd
import sys
import json
import os
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from oauth2client.service_account import ServiceAccountCredentials
from dotenv import load_dotenv
from datetime import datetime
from apiclient.http import MediaFileUpload

load_dotenv()

SCOPES = ['https://www.googleapis.com/auth/drive.metadata.readonly', 'https://www.googleapis.com/auth/drive']

def get_sheets_client(credentials):
	# use creds to create a client to interact with the Google Drive API
	sheets_scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
	sheets_creds = ServiceAccountCredentials.from_json_keyfile_dict(credentials, sheets_scope)
	sheets_client = gspread.authorize(sheets_creds)
	return sheets_client

def accuracy_extract(sheets_client,tab,num_worksheet,type_extract,col_num,new_folder_id):
    # fetch all records
    records = tab.get_all_records()
    # fetch rows based on the accurate value
    acc = []
    inacc = []
    if type_extract == 'entities':
        for row in records:
            if row['accurate_entity'].lower() == 'yes' and row['accurate_type'].lower() == 'yes' and row['accurate_mentions'].lower() == 'yes':
                acc.append(row)
            else:
                inacc.append(row)
    else:
        for row in records:
            if row['accurate_fact'].lower() == 'yes' or (row['accurate_subject'].lower() == 'yes' and row['accurate_relation'].lower() == 'yes' and row['accurate_object'].lower() == 'yes'):
                acc.append(row)
            else:
                inacc.append(row)

    # adding new worksheets, one for accurate and another for inaccurate type
    sheet_acc = sheets_client.create('accurate_{}.tsv'.format(type_extract), folder_id= new_folder_id)
    sheet_acc.add_worksheet(rows=500, cols=17, title = 'accurate_{}'.format(type_extract))
    
    sheet_inacc = sheets_client.create('inaccurate_{}.tsv'.format(type_extract), folder_id= new_folder_id)
    sheet_inacc.add_worksheet(rows=500, cols=17, title = 'inaccurate_{}'.format(type_extract))
    
    sheet_acc.del_worksheet(sheet_acc.worksheet('Sheet1'))
    sheet_inacc.del_worksheet(sheet_inacc.worksheet('Sheet1'))
    
    sheet_acc_tab = sheet_acc.get_worksheet(0)
    sheet_inacc_tab = sheet_inacc.get_worksheet(0)
    
    
    # creating data frame for updating worksheets
    records_acc = pd.DataFrame.from_dict(acc)
    records_inacc = pd.DataFrame.from_dict(inacc)
    # preparing rows for worksheet
    acc_values = records_acc.values.tolist()
    # adding row with column names
    acc_values.insert(0,records_acc.keys().tolist())
    sheet_acc_tab.insert_rows(acc_values)
    # same process for inaccurate values
    inacc_values = records_inacc.values.tolist()
    inacc_values.insert(0,records_inacc.keys().tolist())
    sheet_inacc_tab.insert_rows(inacc_values) 
    
def main():
    # authenticate to access google sheets
    creds = None    

    token_file_str = os.getenv("TOKEN_FILE").replace('\\','')
    token_file = json.loads(token_file_str,strict=False)

    sheets_client = get_sheets_client(token_file)
    folder_id = '1Wbv0NnveBLsKl95V3ryqIPxXLho8PUzs'

    # reading entity name and open sheet
    entity_name = sys.argv[1]
    sheet = sheets_client.open(entity_name)
    #tabs = [sheet.worksheet('accurate_entities'),sheet.worksheet('inaccurate_entities'),sheet.worksheet('accurate_facts'),sheet.worksheet('inaccurate_facts')]
    entity_tab = sheet.worksheet('entities')
    fact_tab = sheet.worksheet('facts')

    # read token file
    creds = ServiceAccountCredentials.from_json_keyfile_dict(token_file, SCOPES)
    drive_service = build('drive', 'v3', credentials=creds)

    # Read folder id
    folder_dets = {}
    with open('folder_dets.json','r') as f:
        folder_dets = json.load(f)
    folder_id = folder_dets['folder_id']

    # create output folder on google drive
    file_metadata = {
    'name': 'output_'+datetime.utcnow().strftime("%m/%d/%Y,%H:%M:%S"),
    'parents': [folder_id],
    'mimeType': 'application/vnd.google-apps.folder'
    }
    file = drive_service.files().create(body=file_metadata,
                                        fields='id').execute()
    new_folder_id = file.get('id')
    
    # upload missed_facts.tsv if it exists
    if os.path.isfile('missed_facts.tsv'):
        file_metadata = {
                'name': "missed_facts.tsv",
                'parents': [new_folder_id]
                }
        media = MediaFileUpload('missed_facts.tsv',
                                mimetype='text/tab-separated-values',
                                        resumable=True)
        
        drive_service.files().create(body=file_metadata,
                                            media_body=media,
                                            fields='id').execute()
    # uploading missed_entities.tsv to Google Drive if it exists
    if os.path.isfile('missed_entities.tsv'):
        file_metadata = {
                'name': "missed_entities.tsv",
                'parents': [new_folder_id]
                }
        media = MediaFileUpload('missed_entities.tsv',
                                mimetype='text/tab-separated-values',
                                        resumable=True)
        
        drive_service.files().create(body=file_metadata,
                                            media_body=media,
                                            fields='id').execute()

    # for entities
    accuracy_extract(sheets_client,entity_tab,2,'entities',15,new_folder_id)
    # for facts
    accuracy_extract(sheets_client,fact_tab,4,'facts',15,new_folder_id)
    '''
    # creating new spreadsheets for each tab
    for tab in tabs:
        filename = str(tab.title) + '.tsv'
        # create spreadsheet
        spreadsheet = sheets_client.create(filename, new_folder_id)
        # get first worksheet of the spreadsheet
        worksheet = spreadsheet.get_worksheet(0)
        worksheet.update_title(str(tab.title))
        # write content
        worksheet.insert_rows(tab.get_all_values())
    '''   
if __name__ == "__main__":
    main()