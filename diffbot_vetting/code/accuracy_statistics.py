
import gspread
from oauth2client.service_account import ServiceAccountCredentials
from pathlib import Path
from pydrive.auth import GoogleAuth
import datetime
import pandas as pd
import sys
from format_sheet import format_after
import json
import os
from googleapiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials
from dotenv import load_dotenv
from googleapiclient.http import MediaIoBaseDownload
import io
import shutil
from apiclient.http import MediaFileUpload

# what if total number of statistics is different for past vet?
SCOPES = ['https://www.googleapis.com/auth/drive.metadata.readonly', 'https://www.googleapis.com/auth/drive']

load_dotenv()
def get_sheets_client(credentials):
	# use creds to create a client to interact with the Google Drive API
	sheets_scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
	sheets_creds = ServiceAccountCredentials.from_json_keyfile_dict(credentials, sheets_scope)
	sheets_client = gspread.authorize(sheets_creds)
	return sheets_client

# this function uploads the statistics.json file to the google drive folder
def upload_json(cur_folder_id,json_dict,drive_service):
    # creating the file locally first
    with open('statistics.json','w') as f:
        json.dump(json_dict,f,indent=4)
    # uploading the file on google drive 
    file_metadata = {
            'name': "statistics.json",
            'parents': [cur_folder_id]
            }
    media = MediaFileUpload("statistics.json",
                            mimetype='application/json',
                                    resumable=True)
    
    drive_service.files().create(body=file_metadata,
                                        media_body=media,
                                        fields='id').execute()
    
    # deleting unnecessary files from local system
    if os.path.exists('compare_to_statistics.json'):
        os.remove('compare_to_statistics.json')
    os.remove('statistics.json')
    os.remove('folder_dets.json')
    if os.path.exists('missed_facts.tsv'):
        os.remove('missed_facts.tsv')
    if os.path.exists('missed_entities.tsv'):
        os.remove('missed_entities.tsv')

# This function calculates the precision of for facts and entities 
# and returns it as a ratio : accurate/total_records

def calc_precision(ent_tab,fact_tab):
    ent_records = ent_tab.get_all_records()
    fact_records = fact_tab.get_all_records()
    acc_ent = 0
    acc_type = 0
    acc_mentions = 0
    for row in ent_records:
        if row['accurate_entity'].lower() == 'yes':  
            acc_ent = acc_ent + 1
        if row['accurate_type'].lower() == 'yes': 
            acc_type = acc_type + 1
        if row['accurate_mentions'].lower() == 'yes':
            acc_mentions = acc_mentions + 1
    
    acc_sub = 0
    acc_rel = 0
    acc_obj = 0
    acc_fact = 0
    for row in fact_records:
        if row['accurate_subject'].lower() == 'yes':
            acc_sub = acc_sub + 1
        if row['accurate_relation'].lower() == 'yes':
            acc_rel = acc_rel + 1
        if row['accurate_object'].lower() == 'yes':
            acc_obj = acc_obj + 1
        if row['accurate_fact'].lower() == 'yes' or (row['accurate_subject'].lower() == 'yes' and row['accurate_relation'].lower() == 'yes' and row['accurate_object'].lower() == 'yes'):
            acc_fact = acc_fact + 1
    
    total_ent = len(ent_records)
    total_fact = len(fact_records)
    
    ent_ratio = "{0}/{1}".format(acc_ent,total_ent)
    type_ratio = "{0}/{1}".format(acc_type,total_ent)
    ment_ratio = "{0}/{1}".format(acc_mentions,total_ent)
    sub_ratio = "{0}/{1}".format(acc_sub,total_fact)
    rel_ratio = "{0}/{1}".format(acc_rel,total_fact)
    obj_ratio = "{0}/{1}".format(acc_obj,total_fact)
    fact_ratio = "{0}/{1}".format(acc_fact,total_fact)

    return ent_ratio,type_ratio,ment_ratio,sub_ratio,rel_ratio,obj_ratio,fact_ratio

# this function compares the precision of the current vet with a past vet
def compare_accuracy(drive_service,cur_ratios):
    diffbot_folder_id = '1Wbv0NnveBLsKl95V3ryqIPxXLho8PUzs'

    results = drive_service.files().list(q = "'{}' in parents".format(diffbot_folder_id), pageSize=10, fields="nextPageToken, files(id, name)").execute()
    items = results.get('files', [])
    # finding the id of the past vet folder
    folder_id = None
    for item in items:
        if item['name'] == sys.argv[2]:
            folder_id = item['id']

    if not folder_id:
        print("No such folder for comparing accuracy exists.")
        exit()

    results = drive_service.files().list(q = "'{}' in parents".format(folder_id), pageSize=10, fields="nextPageToken, files(id, name)").execute()
    items = results.get('files', [])
    # finding the id of the statistics file
    statistics_file_id = None
    for item in items:
        if 'statistics' in item['name']:
            statistics_file_id = item['id']
    
    if not statistics_file_id:
        print("Provided folder does not have a statistics file to compare with. Create a statistics file for that folder first.")
        exit()

    # downloading the statistics file of the past vet onto the local system.
    request = drive_service.files().get_media(fileId=statistics_file_id)
    fh = io.BytesIO()
    downloader = MediaIoBaseDownload(fh, request)
    done = False
    while done is False:
        status, done = downloader.next_chunk()
    fh.seek(0)
    
    with open('compare_to_statistics.json', 'wb') as f:
        shutil.copyfileobj(fh, f, length=131072)
    
    with open('compare_to_statistics.json', 'r') as f:
        json_dict = json.load(f)
    
    # getting statistics from the past file json object
    comp_ent_ratio = json_dict['vetting_details']['current_vetting']['entities']['accurate_entities']
    comp_type_ratio = json_dict['vetting_details']['current_vetting']['entities']['accurate_types']
    comp_ment_ratio = json_dict['vetting_details']['current_vetting']['entities']['accurate_mentions']
    comp_sub_ratio = json_dict['vetting_details']['current_vetting']['facts']['accurate_subjects']
    comp_rel_ratio = json_dict['vetting_details']['current_vetting']['facts']['accurate_relations']
    comp_obj_ratio = json_dict['vetting_details']['current_vetting']['facts']['accurate_objects']
    comp_fact_ratio = json_dict['vetting_details']['current_vetting']['facts']['accurate_facts']

    # calculating difference between current vet and past vet statistics.
    # splitting based on '/' as ratio is a string from which we only need to extract the numerator
    diff_ent = int(cur_ratios[0].split('/')[0]) - int(comp_ent_ratio.split('/')[0])
    diff_type = int(cur_ratios[1].split('/')[0]) - int(comp_type_ratio.split('/')[0])
    diff_ment = int(cur_ratios[2].split('/')[0]) - int(comp_ment_ratio.split('/')[0])
    diff_sub =  int(cur_ratios[3].split('/')[0]) - int(comp_sub_ratio.split('/')[0])
    diff_rel =  int(cur_ratios[4].split('/')[0]) - int(comp_rel_ratio.split('/')[0])
    diff_obj =  int(cur_ratios[5].split('/')[0]) - int(comp_obj_ratio.split('/')[0])
    diff_fact = int(cur_ratios[6].split('/')[0]) - int(comp_fact_ratio.split('/')[0])
    
    return diff_ent,diff_type,diff_ment,diff_sub,diff_rel,diff_obj,diff_fact

def main():
    creds = None
    # getting hidden token from env file
    token_file_str = os.getenv("TOKEN_FILE").replace('\\','')
    token_file = json.loads(token_file_str,strict=False)

    creds = ServiceAccountCredentials.from_json_keyfile_dict(token_file, SCOPES)
    drive_service = build('drive', 'v3', credentials=creds)

    sheets_client = get_sheets_client(token_file)
    sheet = sheets_client.open(sys.argv[1])
    ent_tab = sheet.worksheet('entities')
    fact_tab = sheet.worksheet('facts')
    
    # getting the id of the current folder in the Diffbot_Vetting folder
    folder_dets = {}
    with open('folder_dets.json','r') as f:
        folder_dets = json.load(f)
    cur_folder_id = folder_dets['folder_id']
    cur_folder_name = folder_dets['folder_name']
    date_diffbot = datetime.datetime.now().strftime("%m/%d/%Y,%H:%M:%S")
    # defining the json dictionary
    json_dict = {"vetting_details":{"current_vetting":{"Folder_Name":cur_folder_name,"Date":date_diffbot,"entities":{"accurate_entities":"","accurate_types":"","accurate_mentions":""},"facts":{"accurate_subjects":"","accurate_relations":"","accurate_objects":""}}}}
    # checking if a statistics file already exists for the current folder
    results = drive_service.files().list(q = "'{}' in parents".format(cur_folder_id), pageSize=10, fields="nextPageToken, files(id, name)").execute()
    items = results.get('files', [])
    
    new_file = True
    for item in items:
        if 'statistics' in item['name']:
            # if a statistics file already exists, download it
            request = drive_service.files().get_media(fileId=item['id'])
            fh = io.BytesIO()
            downloader = MediaIoBaseDownload(fh, request)
            done = False
            while done is False:
                status, done = downloader.next_chunk()
                print("Download %d%%" % int(status.progress() * 100))
            fh.seek(0)
            new_file = False
            with open('statistics.json', 'wb') as f:
                shutil.copyfileobj(fh, f, length=131072)
            drive_service.files().delete(fileId=item['id']).execute()
            with open('statistics.json', 'r') as f:
                json_dict = json.load(f)
            break

    ent_ratio,type_ratio,ment_ratio,sub_ratio,rel_ratio,obj_ratio,fact_ratio = calc_precision(ent_tab,fact_tab)
    
    # storing calculated statistics in json_dict 
    current_vet = json_dict['vetting_details']['current_vetting']
    current_vet['entities']['accurate_entities'] = ent_ratio
    current_vet['entities']['accurate_types'] = type_ratio
    current_vet['entities']['accurate_mentions'] = ment_ratio
    current_vet['facts']['accurate_subjects'] = sub_ratio
    current_vet['facts']['accurate_relations'] = rel_ratio
    current_vet['facts']['accurate_objects'] = obj_ratio
    current_vet['facts']['accurate_facts'] = fact_ratio
    # checking if an argument for a past vet folder was provided
    try:
        comp_file_name = sys.argv[2]
    except:
        # if no argument was provided, upload json object with only current statistics
        upload_json(cur_folder_id,json_dict,drive_service)
        exit()
    # getting comparison statistics
    diff_ent,diff_type,diff_ment,diff_sub,diff_rel,diff_obj,diff_fact = compare_accuracy(drive_service,[ent_ratio,type_ratio,ment_ratio,sub_ratio,rel_ratio,obj_ratio,fact_ratio])
    compare_vet = json_dict['vetting_details']
    diff_dict = {"compared_with":comp_file_name,"entities":{"improved_entities":diff_ent,"improved_types":diff_type,"improved_mentions":diff_ment},"facts":{"improved_subjects":diff_sub,"improved_relations":diff_rel,"improved_objects":diff_obj,"improved_facts":diff_fact}}
    # if no statistics file exists already or a comaprison has not been done in the past, create compared betting object in json
    if new_file or "compared_vetting" not in compare_vet.keys():
        compare_vet["compared_vetting"] = [diff_dict]
    # else just append to the compare_vet object
    else:
        compare_vet["compared_vetting"].append(diff_dict)
       
    upload_json(cur_folder_id,json_dict,drive_service)


if __name__ == "__main__":
    main()